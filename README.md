# Photon-Particle Program
- - - - 
## Description

* Communicate between Photon Board and MSP430FR5994 Board using I2C 
	- Photon is master
	- MSP430 is slave
* Upload event to cloud
* Trigger Phone call and Text Message by using Webhook

