
#include "application.h"

volatile int pressYellowCount = 0;
volatile int pressGreenCount = 0;
volatile int pressRedCount = 0;
String yellowData;
String greenData;
String redData;
String smsMessage = "The Button Yellow Has Been Pressed 3 Times";
void setup()
{
  Wire.setSpeed(400000);                    // Set clock speed to 400 kHz
  Wire.begin();                             // Join I2C bus as master
  Serial.begin(9600);
  while (!Serial); Particle.process();
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  Particle.variable("YellowData", yellowData);
  Particle.variable("GreenData", greenData);
  Particle.variable("RedData", redData);
}

void loop(){
  volatile char c;
  /*----- Request TXData from Slave ------*/
  Wire.requestFrom(72, 1, true);
  while (Wire.available()){
    c = Wire.read();
  }
  delay(100);

  if (c == 'Y') {
    yellowData = String(random(0, 100));
    pressYellowCount++;
    Particle.publish("YELLOW", yellowData, PRIVATE);
    digitalWrite(D2, HIGH);
    delay(100);
    digitalWrite(D2, LOW);
  } else if (c == 'G') {
    greenData = String(random(0, 100));
    pressGreenCount++;
    Particle.publish("GREEN",greenData,60,PRIVATE);
    digitalWrite(D3, HIGH);
    delay(100);
    digitalWrite(D3, LOW);
  } else if (c == 'R') {
    redData = String(random(0, 100));
    pressRedCount++;
    Particle.publish("RED",redData,60,PRIVATE);
    digitalWrite(D4, HIGH);
    delay(100);
    digitalWrite(D4, LOW);
  }
  if (pressYellowCount >= 3) {
    pressYellowCount = 0;
    Particle.publish("sms/4022158946", smsMessage, PRIVATE);
  } else if (pressYellowCount >= 1 && pressGreenCount >= 2 && pressRedCount >= 3) {
    pressYellowCount = 0;
    pressGreenCount = 0;
    pressRedCount = 0;
    //Particle.publish("call/4022158946", "Hello There, Seems Like your system is about to have some problem with power transfering. Please Call Conductix at 4,0,2, 3,3,9, 9,3,0,0, To Replace Conductor Shoes!", PRIVATE);
  }
}
